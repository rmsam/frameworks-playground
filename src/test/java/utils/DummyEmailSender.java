package utils;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

public class DummyEmailSender {
  public static final String SUBJECT = "Test";
  public static final Integer PORT = 2500;
  public static final String HOST = "localhost";
  public static final String SENDER = "sender@here.com";
  public static final String RECEIVER = "receiver@there.com";

  private Properties mailProps;

  public DummyEmailSender() {
    mailProps = new Properties();
    mailProps.setProperty("mail.smtp.host", HOST);
    mailProps.setProperty("mail.smtp.port", PORT.toString());
  }

  public void sendEmailMessageToDefaultRecipient(String messageBody) throws MessagingException, UnsupportedEncodingException {
    Session session = Session.getDefaultInstance(mailProps, null);
    MimeMessage msg = new MimeMessage(session);

    msg.setFrom(new InternetAddress(SENDER));
    msg.setSubject(SUBJECT);
    msg.setSentDate(new Date());
    String mimeEncodedText = MimeUtility.encodeText(messageBody);
    msg.setText(mimeEncodedText);
    msg.setRecipient(Message.RecipientType.TO, new InternetAddress(RECEIVER));

    Transport.send(msg);

  }
}
