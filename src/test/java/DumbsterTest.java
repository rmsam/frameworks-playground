import static org.junit.Assert.assertEquals;

import com.dumbster.smtp.SimpleSmtpServer;
import com.dumbster.smtp.SmtpMessage;
import org.junit.Test;
import utils.DummyEmailSender;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeUtility;

public class DumbsterTest {

  private static final String MESSAGE_BODY_WITH_LATIN_CHARS = "Pastéis de Nata";
  private static final String MESSAGE_BODY_WITHOUT_LATIN_CHARS = "Custard Tart";
  private static final String MESSAGE_BODY_IS_XML = "<p>Hi Mr <b>Custard Tart</b></p>";

  @Test
  public void correctEmailShouldBeCapturedWhenBodyContainsLatinChars() throws MessagingException,
      UnsupportedEncodingException {
    sendAndAssertEmail(MESSAGE_BODY_WITH_LATIN_CHARS);
  }

  @Test
  public void correctEmailShouldBeCapturedWhenBodyDoesntContainLatinChars() throws MessagingException,
      UnsupportedEncodingException {
    sendAndAssertEmail(MESSAGE_BODY_WITHOUT_LATIN_CHARS);
  }

  @Test
  public void correctEmailShouldBeCapturedWhenBodyIsAnXml() throws MessagingException,
      UnsupportedEncodingException {
    sendAndAssertEmail(MESSAGE_BODY_IS_XML);
  }

  private void sendAndAssertEmail(String messageBody) throws MessagingException, UnsupportedEncodingException {
    SimpleSmtpServer server = SimpleSmtpServer.start(DummyEmailSender.PORT);
    new DummyEmailSender().sendEmailMessageToDefaultRecipient(messageBody);
    server.stop();

    assertEquals(1, server.getReceivedEmailSize());

    SmtpMessage email = (SmtpMessage) server.getReceivedEmail().next();
    assertEquals(DummyEmailSender.SUBJECT, getSubject(email));
    String decodedBody = MimeUtility.decodeText(email.getBody());
    assertEquals(messageBody, decodedBody);
  }

  private String getSubject(SmtpMessage email) {
    return email.getHeaderValue("Subject");
  }
}
